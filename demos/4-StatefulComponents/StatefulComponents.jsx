import React, { useState } from 'react';
import { View, Text, Button } from 'react-native';

const GREETINGS_LIST = ['Bonjour', 'Salut', 'Hello', 'Ça va ?']

const getRandomItem = (array) => {
	return array[Math.floor(Math.random() * array.length)];
}

const StatefulComponent = () => {
	const [greetings, setGreetings] = useState(getRandomItem(GREETINGS_LIST))
	const sayHello = () => {
		const newGreetings = getRandomItem(GREETINGS_LIST)
		console.log(`sayHello, appel setGreetings("${newGreetings}")`);
		setGreetings(newGreetings)
	}

	const [count, setCount] = useState(1)
	const incrementCount = () => {
		console.log('incrementCount, appel setCount');
		setCount(previousState => previousState + 1)
	}

	console.log('nouveau rendu', {greetings, count});

	return (
		<View>
			<Button
				onPress={sayHello}
				title="Coucou"
			/>
			<Text style={centeredText}>{greetings}</Text>

			<Button
				onPress={incrementCount}
				title="Augmenter le compteur"
			/>
			<Text style={centeredText}>{count}</Text>
		</View>
	);
}

const centeredText = { textAlign: 'center', fontSize: 24, margin: 12 }

export default StatefulComponent

import { useState } from "react";
import { Text, View, TextInput, Button } from 'react-native';

export default function TaskList() {
	const [inputValue, setInputValue] = useState('')
	const [tasks, setTasks] = useState([])

	const onSubmit = (event) => {
		setTasks([
			...tasks,
			inputValue
		])
		setInputValue('')
	}

	return (
		<View style={{ padding: 10 }}>
			{/* afficher un formulaire */}
			<Text style={{ fontSize: 20 }}>Nouvelle tâche</Text>
			<View style={{ flexDirection: "row"}}>
				<TextInput
					onChangeText={(text) => {
						setInputValue(text)
					}}
					value={inputValue}
					style={{
						flexGrow: 1,
						borderWidth: 1,
						padding: 5,
						fontSize: 20,
						borderColor: "#000"
					}}
				/>
				<Button
					onPress={onSubmit}
					title="Ajouter"
				/>
			</View>

			<Text style={{
				marginVertical: 10,
				fontSize: 26,
				fontWeight: "bold"
			}}>
				Liste de tâches
			</Text>

			{tasks.map((task, index) => (
				<Text style={{fontSize: 20}} key={index}>{task}</Text>
			))}
		</View>
	);
}
